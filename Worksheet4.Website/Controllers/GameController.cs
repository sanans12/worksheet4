﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Worksheet4.Website.Models;
using Worksheet4.Website.Services.Implementations;

namespace Worksheet4.Website.Controllers
{
    public class GameController : Controller
    {
        private readonly IGameFacadeService _gameFacadeService;

        public GameController(IGameFacadeService gameFacadeService)
        {
            _gameFacadeService = gameFacadeService;
        }

        // GET: Games
        public async Task<IActionResult> Index()
        {
            IEnumerable<GameViewModel> gameViewModels = await _gameFacadeService.GetAll();

            return View(gameViewModels);
        }

        // GET: Games/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            GameViewModel gameViewModel = await _gameFacadeService.GetById(id);

            return View(gameViewModel);
        }

        // GET: Games/Create
        public async Task<IActionResult> Create()
        {
            return View();
        }

        // POST: Games/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name, Price")] GameViewModel gameViewModel)
        {
            if (ModelState.IsValid)
            {
                bool isSuccessful = await _gameFacadeService.Create(gameViewModel);

                if (isSuccessful)
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(gameViewModel);
        }

        // GET: Games/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            GameViewModel gameViewModel = await _gameFacadeService.GetById(id);

            return View(gameViewModel);
        }

        // POST: Games/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Name, Price")] GameViewModel gameViewModel)
        {
            if (id != gameViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                bool isSuccessful = await _gameFacadeService.Update(gameViewModel);

                if (isSuccessful)
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(gameViewModel);
        }

        // GET: Games/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            GameViewModel gameViewModel = await _gameFacadeService.GetById(id);

            return View(gameViewModel);
        }

        // POST: Games/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, [Bind("Id, Name, Price")] GameViewModel gameViewModel)
        {
            if (id != gameViewModel.Id)
            {
                return NotFound();
            }

            bool isSuccessful = await _gameFacadeService.Delete(id);

            if (isSuccessful)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(gameViewModel);
        }
    }
}