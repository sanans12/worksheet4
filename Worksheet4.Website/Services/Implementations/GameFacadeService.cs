﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Flurl;
using Flurl.Http;
using Microsoft.AspNetCore.Http;
using Worksheet4.Models;
using Worksheet4.Website.Models;

namespace Worksheet4.Website.Services.Implementations
{
    public class GameFacadeService : IGameFacadeService
    {
        private readonly IMapper _mapper;

        /// <summary>
        /// The API endpoint this service uses.
        /// </summary>
        private const string API_URL = "http://localhost:54749/api";

        public GameFacadeService(IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// Sends a POST message to the API with a JSON of the DTO to create an entity.
        /// </summary>
        /// <param name="gameViewModel"></param>
        /// <returns></returns>
        public async Task<bool> Create(GameViewModel gameViewModel)
        {
            if (gameViewModel == null)
            {
                throw new ArgumentNullException();
            }

            GameDTO gameDTO = _mapper.Map<GameDTO>(gameViewModel);

            HttpResponseMessage response = await API_URL.AppendPathSegment("game").PostJsonAsync(gameDTO);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sends a GET message to the API with no parameters in order to retrieve all DTOs.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<GameViewModel>> GetAll()
        {
            HttpResponseMessage response = await API_URL.AppendPathSegment("games").GetAsync();

            IEnumerable<GameDTO> gameDTOs = await response.Content.ReadAsAsync<IEnumerable<GameDTO>>();

            return _mapper.Map<IEnumerable<GameViewModel>>(gameDTOs);
        }

        /// <summary>
        /// Sends a GET message to the API with a ID parameter to retrieve the relevant DTO.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GameViewModel> GetById(int? id)
        {
            if (!id.HasValue)
            {
                throw new ArgumentNullException();
            }

            Url url = API_URL.AppendPathSegment("game").SetQueryParam("id", id);

            HttpResponseMessage response = await url.GetAsync();

            GameDTO gameDTO = await response.Content.ReadAsAsync<GameDTO>();

            return _mapper.Map<GameViewModel>(gameDTO);
        }

        /// <summary>
        /// Sends a PUT message to the API with a DTO to use to update an entity.
        /// </summary>
        /// <param name="gameViewModel"></param>
        /// <returns></returns>
        public async Task<bool> Update(GameViewModel gameViewModel)
        {
            if (gameViewModel == null)
            {
                throw new ArgumentNullException();
            }

            GameDTO gameDTO = _mapper.Map<GameDTO>(gameViewModel);

            HttpResponseMessage response = await API_URL.AppendPathSegment("game").PutJsonAsync(gameDTO);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sends a DELETE message to the API with an id to locate the entity to delete.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> Delete(int? id)
        {
            if (!id.HasValue)
            {
                throw new ArgumentNullException();
            }

            Url url = API_URL.SetQueryParam("id", id);

            HttpResponseMessage response = await url.AppendPathSegment("game").DeleteAsync();

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
    }
}
