﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Worksheet4.Models;
using Worksheet4.Website.Models;

namespace Worksheet4.Website.Services.Implementations
{
    public interface IGameFacadeService
    {
        /// <summary>
        /// Sends a POST message to the API with a JSON of the DTO to create an entity.
        /// </summary>
        /// <param name="gameViewModel"></param>
        /// <returns></returns>
        Task<bool> Create(GameViewModel gameViewModel);

        /// <summary>
        /// Sends a GET message to the API with no parameters in order to retrieve all DTOs.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<GameViewModel>> GetAll();

        /// <summary>
        /// Sends a GET message to the API with a ID parameter to retrieve the relevant DTO.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GameViewModel> GetById(int? id);

        /// <summary>
        /// Sends a PUT message to the API with a DTO to use to update an entity.
        /// </summary>
        /// <param name="gameViewModel"></param>
        /// <returns></returns>
        Task<bool> Update(GameViewModel gameViewModel);

        /// <summary>
        /// Sends a DELETE message to the API with an id to locate the entity to delete.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(int? id);
    }
}