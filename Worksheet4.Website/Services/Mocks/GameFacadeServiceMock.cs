﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Worksheet4.Models;
using Worksheet4.Website.Models;
using Worksheet4.Website.Services.Implementations;

namespace Worksheet4.Website.Services.Mocks
{
    public class GameFacadeServiceMock : IGameFacadeService
    {
        private readonly IMapper _mapper;

        private readonly ICollection<GameViewModel> _gameViewModels;

        public GameFacadeServiceMock(IMapper mapper)
        {
            _mapper = mapper;

            _gameViewModels = new List<GameViewModel>()
            {
                new GameViewModel { Id = 1, Name = "Fakemon Go!", Price = 9.99 },
                new GameViewModel { Id = 2, Name = "Draw your Fake Friends", Price = 99.99 },
                new GameViewModel { Id = 3, Name = "Mystery Fake Box Tycoon", Price = 300.00 },
                new GameViewModel { Id = 4, Name = "Faking Papa", Price = 499.99 }
            };
        }

        public async Task<bool> Create(GameViewModel gameViewModel)
        {
            _gameViewModels.Add(gameViewModel);

            return true;
        }

        public async Task<IEnumerable<GameViewModel>> GetAll()
        {
            return _gameViewModels;
        }

        public async Task<GameViewModel> GetById(int? id)
        {
            return _gameViewModels.Single(x => x.Id == id);
        }

        public async Task<bool> Update(GameViewModel gameViewModel)
        {
            GameViewModel gameViewModelToRemove = _gameViewModels.Single(x => x.Id == gameViewModel.Id);

            _gameViewModels.Remove(gameViewModelToRemove);

            _gameViewModels.Add(gameViewModel);

            return true;
        }

        public async Task<bool> Delete(int? id)
        {
            GameViewModel gameViewModelToRemove = _gameViewModels.Single(x => x.Id == id);

            _gameViewModels.Remove(gameViewModelToRemove);

            return true;
        }
    }
}
