﻿using AutoMapper;
using Worksheet4.Models;
using Worksheet4.Website.Models;

namespace Worksheet4.Website
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<GameDTO, GameViewModel>().ReverseMap();
        }
    }
}
