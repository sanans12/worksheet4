﻿using System;

namespace Worksheet4.Models
{
    public class GameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
