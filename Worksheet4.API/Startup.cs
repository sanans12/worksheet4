using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Worksheet4.Data;
using AutoMapper;
using Worksheet4.API.Services.Implementations;
using Worksheet4.API.Services.Interfaces;
using Worksheet4.API.Services.Mocks;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace Worksheet4.API
{
    public class Startup
    {
        private const bool SHOULD_MOCK_GAME_REPOSITORY = false;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            if (SHOULD_MOCK_GAME_REPOSITORY)
            {
                services.AddSingleton<IGameRepositoryService, GameRepositoryServiceMock>();
            }
            else
            {
                services.AddScoped<IGameRepositoryService, GameRepositoryService>();
            }

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<GameContext>(options => options.UseInMemoryDatabase(databaseName: "Worksheet4"));

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Game CRUD API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Game CRUD API");
                options.RoutePrefix = "api";
            });
        }
    }
}
