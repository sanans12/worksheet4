﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Worksheet4.API.Services.Interfaces;
using Worksheet4.Data;
using Worksheet4.Models;

namespace Worksheet4.API.Services.Implementations
{
    public class GameRepositoryService : IGameRepositoryService
    {

        private readonly GameContext _context;
        private readonly IMapper _mapper;

        public GameRepositoryService(GameContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates a member in the database from the DTO.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        public async Task CreateGame(GameDTO gameDTO)
        {
            Game game = _mapper.Map<Game>(gameDTO);

            _context.Games.Add(game);

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Returns a collection of DTOs.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<GameDTO>> GetGames()
        {
            IEnumerable<Game> games = _context.Games.Where(game => game.Active);

            return _mapper.Map<IEnumerable<GameDTO>>(games);
        }

        /// <summary>
        /// Returns a DTO with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GameDTO> GetGame(int id)
        {
            Game game = _context.Games.Where(game => game.Active).Single(game => game.Id == id);

            return _mapper.Map<GameDTO>(game);
        }

        /// <summary>
        /// Updates the DTOs equivalent entity in the database.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        public async Task UpdateGame(GameDTO gameDTO)
        {
            Game game = _mapper.Map<Game>(gameDTO);

            _context.Entry(game).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes an entity in the database with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteGame(int id)
        {
            Game game = await _context.Games.FindAsync(id);

            game.Active = false;

            _context.Entry(game).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }
    }
}
