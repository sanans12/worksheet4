﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Worksheet4.API.Services.Interfaces;
using Worksheet4.Data;
using Worksheet4.Models;

namespace Worksheet4.API.Services.Mocks
{
    public class GameRepositoryServiceMock : IGameRepositoryService
    {
        private readonly IMapper _mapper;

        private readonly ICollection<GameDTO> _gameDTOs;

        public GameRepositoryServiceMock(IMapper mapper)
        {
            _mapper = mapper;

            _gameDTOs = new List<GameDTO>()
            {
                new GameDTO { Id = 1, Name = "Fakemon Go!", Price = 9.99 },
                new GameDTO { Id = 2, Name = "Draw your Fake Friends", Price = 99.99 },
                new GameDTO { Id = 3, Name = "Mystery Fake Box Tycoon", Price = 300.00 },
                new GameDTO { Id = 4, Name = "Faking Papa", Price = 499.99 }
            };
        }

        /// <summary>
        /// Creates a member in the database from the DTO.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        public async Task CreateGame(GameDTO gameDTO)
        {
            _gameDTOs.Add(gameDTO);
        }

        /// <summary>
        /// Returns a collection of DTOs.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<GameDTO>> GetGames()
        {
            return _gameDTOs;
        }

        /// <summary>
        /// Returns a DTO with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GameDTO> GetGame(int id)
        {
            return _gameDTOs.Single(x => x.Id == id);
        }

        /// <summary>
        /// Updates the DTOs equivalent entity in the database.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        public async Task UpdateGame(GameDTO gameDTO)
        {
            GameDTO gameDTOToRemove = _gameDTOs.Single(x => x.Id == gameDTO.Id);

            _gameDTOs.Remove(gameDTOToRemove);

            _gameDTOs.Add(gameDTO);
        }

        /// <summary>
        /// Deletes an entity in the database with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteGame(int id)
        {
            GameDTO gameDTOToRemove = _gameDTOs.Single(x => x.Id == id);

            _gameDTOs.Remove(gameDTOToRemove);
        }
    }
}
