﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Worksheet4.Models;

namespace Worksheet4.API.Services.Interfaces
{
    public interface IGameRepositoryService
    {
        /// <summary>
        /// Creates a member in the database from the DTO.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        Task CreateGame(GameDTO gameDTO);

        /// <summary>
        /// Returns a collection of DTOs.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<GameDTO>> GetGames();

        /// <summary>
        /// Returns a DTO with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GameDTO> GetGame(int id);

        /// <summary>
        /// Updates the DTOs equivalent entity in the database.
        /// </summary>
        /// <param name="gameDTO"></param>
        /// <returns></returns>
        Task UpdateGame(GameDTO gameDTO);

        /// <summary>
        /// Deletes an entity in the database with a matching ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteGame(int id);
    }
}