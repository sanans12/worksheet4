﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Worksheet4.API.Services.Interfaces;
using Worksheet4.Models;

namespace Worksheet4.API.Controllers
{
    [ApiController]
    [Route("api")]
    public class GameCRUDController : ControllerBase
    {
        private readonly IGameRepositoryService _gameRepositoryService;

        public GameCRUDController(IGameRepositoryService gameRepositoryService)
        {
            _gameRepositoryService = gameRepositoryService;
        }

        [HttpPost("Game")]
        public async Task<IActionResult> Create(GameDTO gameDTO)
        {
            if (gameDTO == null)
            {
                return BadRequest();
            }

            await _gameRepositoryService.CreateGame(gameDTO);

            return CreatedAtAction(nameof(GetById), new { id = gameDTO.Id }, gameDTO);
        }

        [HttpGet("Games")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _gameRepositoryService.GetGames());
        }

        [HttpGet("Game")]
        public async Task<IActionResult> GetById(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }

            try
            {
                return Ok(await _gameRepositoryService.GetGame(id.Value));
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPut("Game")]
        public async Task<IActionResult> Update(GameDTO gameDTO)
        {
            if (gameDTO == null)
            {
                return BadRequest();
            }

            try
            {
                await _gameRepositoryService.UpdateGame(gameDTO);
            }
            catch
            {
                return NotFound();
            }


            return NoContent();
        }

        [HttpDelete("Game")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }

            try
            {
                await _gameRepositoryService.DeleteGame(id.Value);
            }
            catch
            {
                return NotFound();
            }


            return NoContent();
        }
    }
}
