﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Worksheet4.Data;
using Worksheet4.Models;

namespace Worksheet4.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Game, GameDTO>().ReverseMap().AfterMap((dto, game) => game.Active = true);
        }
    }
}
